# Cameras #

## Holga 120GN Camera ##
Was NZD $99.75, now NZD $78.50

For those that prefer a Glass lens version of the classic 120N this Holga fits the bill! Comes with all the standard features that the standard Holga has including Zone Focus, Bulb Mode, Two apertures and two film formats (6×6 and 6×4.5). Some say the Glass lens has a slightly sharper image with more vivid colors but others prefer the classic plastic.

Takes your pick! Add a Basic Lens Kit which includes the Wide angle, Telephoto lens and a Fisheye lens or a Super Lens Kit which includes these three and an additional Close up Lens set and a Macro Lens Kit!

## Fed 3 ##
NZD $620.00

Make memories last a lifetime with the Fed 3 camera. The Industar-61 lens of this Fed rangefinder camera delivers crisp and clear image outputs.

## Kodak Z1015 ##
NZD $560.00

Who says you can't have it all? The Kodak EASYSHARE Z1015 IS Digital Camera brings the picture-taking technology right to your fingertips.

## Leica M-E 18MPx Body ##
NZD $7,998.00

The M-E Digital Rangefinder Camera from Leica is a full frame, interchangeable lens camera that embodies the notion that less is more.

# Digital Holga Lenses #

* Digital Holga Kitchen Sink Kit - Was $174.99, now $89.99
* Digital Holga Ultimate Kit - $69.99
* Digital Holga Starter Kit - Wide to Tele - $51.49
* Digital Holga Starter Kit - Close up And Macro - was $79.99, now $75.99
